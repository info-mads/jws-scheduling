from solvers.solver import Blackouts, Images, Whitespace, TimeslotStarts, TimeslotLengths
import numpy as np


def interval(s: str):
    split = s.split(', ')
    return float(split[0]), float(split[1])


def parse(parsable: str) -> tuple[Images, TimeslotLengths, TimeslotStarts]:
    split = parsable.rstrip().split('\n')
    n_images = int(split[0])
    images = np.asarray(split[1:n_images + 1]).astype(np.float)
    blackouts = map(interval, split[n_images + 2:])
    image_length = sum(images)
    whitespaces = blackout_to_whitespace(blackouts, image_length)
    whitespaces_lengths = calculate_whitespace_length(whitespaces)
    whitespace_starts = calculate_whitespace_starts(whitespaces)
    # return scale(images), scale(whitespaces_lengths), scale(whitespace_starts)
    return images, whitespaces_lengths, whitespace_starts

def add_to_second(x):
    a, b = x
    return a, a + b


def blackout_to_whitespace(blackouts: Blackouts, max_length: int) -> Whitespace:
    new_map = list(map(add_to_second, blackouts))
    new_map.insert(0, (0, 0))
    _, last_blackout_end = new_map[-1]
    new_map.append((last_blackout_end + max_length, 0))
    result = []
    for i in range(len(new_map) - 1):
        a, b = new_map[i]
        c, d = new_map[i + 1]
        result.append((b, c))

    return result


def calculate_whitespace_length(whitespace):
    whitespaces_lengths = []
    for interval in whitespace:
        whitespaces_lengths.append(interval[1] - interval[0])
    return whitespaces_lengths

def calculate_whitespace_starts(whitespaces):
    return list(map(lambda x: x[0], whitespaces))

def scale(arr):
    return list(map(lambda x: int(x * 1000), arr))
