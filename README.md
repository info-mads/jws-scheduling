# JWS Scheduling

## Getting started

- `python3 -m venv env`
- `source env/bin/activate`
- `pip install -r requirements.txt`
- `python3 main.py {input-file-location} {solver:[lp/ilp]}`

## When adding dependencies

- Have source activated, `pip install {dependency}`
- `pip freeze > requirements.txt`
- Deactivate source with `deactivate`
- Git add, commit and push requirements.txt
