from parsers.offline_parser import parse
import sys

from solvers.ortools_solver import OrtoolsSolver
from solvers.ortools_solver_lp import OrtoolsSolverLp

solvers = {
    'ilp': OrtoolsSolver(),
    'lp': OrtoolsSolverLp()
}

if len(sys.argv) < 2:
    raise Exception("Please provide a single file as input")

if len(sys.argv) < 3:
    raise Exception("Please provide a solver name as input")

if not sys.argv[2] in solvers.keys():
    raise Exception(f"Unknown solver. Available solvers: {list(solvers.keys())}")

with open(sys.argv[1], 'r') as file:
    parsable = file.read()
    images, whitespaceLengths, whitespaceStarts = parse(parsable)

    solver = solvers[sys.argv[2]]

    res = solver.solve(images, whitespaceLengths, whitespaceStarts)
    print('\n'.join(map(str, res)))
