from typing import Tuple

Images = list[float]
Blackouts = list[Tuple[float, float]]
Whitespace = Blackouts
TimeslotLengths = list[float]
TimeslotStarts = list[float]
TransmitResult = list[float]


class SolverInterface:
    def solve(self, P: Images, T: TimeslotLengths, St: TimeslotStarts): TransmitResult

    """ Interface method for all solvers to implement """
    pass
