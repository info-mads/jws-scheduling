from parsers.offline_parser import scale
from solvers.solver import SolverInterface, Images, TransmitResult, TimeslotStarts, TimeslotLengths
from ortools.sat.python import cp_model
from ortools.linear_solver import pywraplp


class OrtoolsSolverLp(SolverInterface):
    def solve(self, P: Images, T: TimeslotLengths, St: TimeslotStarts) -> TransmitResult:
        solver = pywraplp.Solver.CreateSolver('SCIP')
        if not solver:
            return []

        # 4
        B = {}
        for image in range(len(P)):
            for timeslot in range(len(T)):
                B[(image, timeslot)] = solver.BoolVar('B_n%id%is' % (image, timeslot))

        # 5
        for image in range(len(P)):
            # An image can only be in exactly one timeslot
            solver.Add(sum(B[(image, timeslot)] for timeslot in range(len(T))) == 1)

        # 6
        for timeslot in range(len(T)):
            images_in_timeslot = []
            for image in range(len(P)):
                images_in_timeslot.append(B[(image, timeslot)] * P[image])
            # All images in timeslot must sum up to less than or equal the timeslot length
            solver.Add(sum(images_in_timeslot) <= T[timeslot])

        # 7
        D = {}
        for timeslot in range(len(T)):
            D[timeslot] = solver.BoolVar('D_%i' % timeslot)

        E = solver.NumVar(0, St[-1] + T[-1], 'upperbound_sol')

        # 8
        for image in range(len(P)):
            # D can only be 0 when there is no image in that timeslot.
            for timeslot in range(len(T)):
                solver.Add(B[(image, timeslot)] <= D[timeslot])

        # 9
        for timeslot in range(len(T)):
            B_sub = [B[(image, timeslot)] * P[image] for image in range(len(P))]
            solver.Add(D[timeslot] * St[timeslot] + sum(B_sub) <= E)

        # Solve
        solver.Minimize(E)

        status = solver.Solve()

        if status != pywraplp.Solver.OPTIMAL:
            print('Solving failed!')
            return []

        result = [
            E.solution_value()
        ]

        image_loc = []
        for image in range(len(P)):
            for timeslot in range(len(T)):
                if B[(image, timeslot)].solution_value():
                    image_loc.append(St[timeslot])
                    St[timeslot] += P[image]

        return result + image_loc
