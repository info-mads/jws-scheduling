from parsers.offline_parser import scale
from solvers.solver import SolverInterface, Images, TransmitResult, TimeslotStarts, TimeslotLengths
from ortools.sat.python import cp_model


class OrtoolsSolver(SolverInterface):
    def solve(self, P: Images, T: TimeslotLengths, St: TimeslotStarts) -> TransmitResult:
        P, T, St = scale(P), scale(T), scale(St)
        model = cp_model.CpModel()

        # 4
        B = {}
        for image in range(len(P)):
            for timeslot in range(len(T)):
                B[(image, timeslot)] = model.NewBoolVar('B_n%id%is' % (image, timeslot))

        # 5
        for image in range(len(P)):
            # An image can only be in exactly one timeslot
            model.AddExactlyOne(B[(image, timeslot)] for timeslot in range(len(T)))

        # 6
        for timeslot in range(len(T)):
            images_in_timeslot = []
            for image in range(len(P)):
                images_in_timeslot.append(B[(image, timeslot)])
            # All images in timeslot must sum up to less than or equal the timeslot length
            model.Add(cp_model.LinearExpr.WeightedSum(images_in_timeslot, P) <= T[timeslot])

        # 7
        D = {}
        for timeslot in range(len(T)):
            D[timeslot] = model.NewBoolVar('D_%i' % timeslot)

        E = model.NewIntVar(0, St[-1] + T[-1], 'upperbound_sol')

        # 8
        for image in range(len(P)):
            # D can only be 0 when there is no image in that timeslot.
            for timeslot in range(len(T)):
                model.Add(B[(image, timeslot)] <= D[timeslot])

        # 9
        for timeslot in range(len(T)):
            B_sub = [B[(image, timeslot)] for image in range(len(P))]
            w_sum = cp_model.LinearExpr.WeightedSum(B_sub, P)
            model.Add(D[timeslot] * St[timeslot] + w_sum <= E)

        # Solve
        model.Minimize(E)

        solver = cp_model.CpSolver()
        status = solver.Solve(model)

        if status != cp_model.OPTIMAL and status != cp_model.FEASIBLE:
            print('Solving failed!')
            return []

        result = [
            solver.ObjectiveValue() / 1000
        ]

        image_loc = []
        for image in range(len(P)):
            for timeslot in range(len(T)):
                if solver.Value(B[(image, timeslot)]):
                    image_loc.append(St[timeslot] / 1000)
                    St[timeslot] += P[image]

        return result + image_loc
